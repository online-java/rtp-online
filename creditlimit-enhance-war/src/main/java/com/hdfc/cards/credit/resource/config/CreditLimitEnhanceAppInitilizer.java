package com.hdfc.cards.credit.resource.config;

import com.hdfc.cards.credit.offers.exception.CleGloabalExceptionHandler;
import com.hdfc.cards.credit.resource.CreditLimitEnhanceResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/v1")
public class CreditLimitEnhanceAppInitilizer extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>>  classes = new HashSet<>();
        classes.add(CreditLimitEnhanceResource.class);
        classes.add(CleGloabalExceptionHandler.class);
        return classes;
    }
}
