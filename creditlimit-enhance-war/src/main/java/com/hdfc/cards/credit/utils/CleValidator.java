package com.hdfc.cards.credit.utils;

import com.hdfc.cards.credit.offers.exception.BusinessException;
import com.hdfc.cards.credit.offers.exception.CleEnum;

public class CleValidator {

    public void validate(String promocode) throws BusinessException {
        if (promocode == null) {

            throw new BusinessException(CleEnum.PROMOCODE_IS_NOT_VALID);
        }
        if ("".equals(promocode)) {
            throw new BusinessException(CleEnum.PROMOCODE_IS_NOT_VALID);

        }


    }
}
