package com.hdfc.cards.credit.utils;

import com.hdfc.cards.credit.offers.exception.BusinessException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4ClassRunner.class)
public class CleValidatorTest {


    private CleValidator cleValidator = null;

    @Before
    public void setUp() throws Exception {
        cleValidator = new CleValidator();
    }

    @Test
    public void validateEmptyPromoCode() {
        String promocode = "";
        String expectedErrorMessage = "promocode is not valid";
        String expectedErrorCode = "CLE1001";

        try {
            cleValidator.validate(promocode);
        } catch (BusinessException e) {
            System.out.println("Positive validation for Empty Promo Code check completed successfully ");
            assertEquals(expectedErrorMessage, e.getErrorMessage());
            assertEquals(expectedErrorCode, e.getErrorCode());

        }
    }

    @Test
    public void validateValidPromoCode() {
        String promocode = "hdfc2020";
        String expectedErrorMessage = "promocode is valid";
        String expectedErrorCode = "CLE1001";

        try {
            cleValidator.validate(promocode);
            System.out.println("Positive validation for valid Promo Code check completed successfully ");
        } catch (BusinessException e) {
            //System.out.println("Positive validation for Empty Promo Code check completed successfully ");
            assertEquals(expectedErrorMessage, e.getErrorMessage());
            assertEquals(expectedErrorCode, e.getErrorCode());

        }
    }

    @After
    public void tearDown() throws Exception {
        cleValidator = null;

    }

}