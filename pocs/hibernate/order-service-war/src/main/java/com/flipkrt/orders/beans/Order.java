package com.flipkrt.orders.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Order
{

    private Integer id;
    private String orderName;
    private String deliveryAddress;

    public Integer getId ()
    {
        return id;
    }

    public void setId (Integer id)
    {
        this.id = id;
    }

    public String getOrderName ()
    {
        return orderName;
    }

    public void setOrderName (String orderName)
    {
        this.orderName = orderName;
    }

    public String getDeliveryAddress ()
    {
        return deliveryAddress;
    }

    public void setDeliveryAddress (String deliveryAddress)
    {
        this.deliveryAddress = deliveryAddress;
    }

    @Override
    public String toString ()
    {
        return "Order{" +
                "id=" + id +
                ", orderName='" + orderName + '\'' +
                ", deliveryAddress='" + deliveryAddress + '\'' +
                '}';
    }
}
