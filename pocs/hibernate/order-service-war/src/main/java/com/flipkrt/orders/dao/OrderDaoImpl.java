package com.flipkrt.orders.dao;

import com.flipkrt.orders.beans.Order;
import com.flipkrt.orders.util.OrderDbConnection;

import java.sql.*;

public class OrderDaoImpl implements OrderDao
{

   private static final String insertquery = "insert into orderinfo values(?,?,?)";
   private static final String getorderquery = "select * from orderinfo where id=?";
   private static final String SelectQuery="Select * from orderinfo";
   private static final String deletequery="delete from orderinfo where id=?";
   private static final String updateQurey="update orderinfo set ordername=? ,deliveryAddress=? where id=?";
    @Override
    public String getAllOrders () throws SQLException
    {
        PreparedStatement preparedStatement=OrderDbConnection.getDbConnection().prepareStatement(SelectQuery);
        ResultSet resultSet=preparedStatement.executeQuery();
        while (resultSet.next())
        {
            System.out.println("ID = "+resultSet.getInt(1)+", Name = "+resultSet.getString(2)+", DeliveryAddress = "+resultSet.getString(3));
        }


        return null;
    }

    @Override
    public String createOrder (Order order) throws SQLException
    {
        
        PreparedStatement preparedStatement = OrderDbConnection.getDbConnection().prepareStatement(insertquery);
        preparedStatement.setInt(1, order.getId());
        preparedStatement.setString(2, order.getOrderName());
        preparedStatement.setString(3, order.getDeliveryAddress());
        String result = null;
        int flag = preparedStatement.executeUpdate();
        if (flag == 1)
        {
            result = "order created successfully";
            System.out.println("order details :" + order);
        } else
        {
            System.out.println("exception while creating order");
        }
        return result;
    }

    @Override
    public  Order getOrder (Integer id) throws SQLException
    {
        
        PreparedStatement preparedStatement = OrderDbConnection.getDbConnection().prepareStatement(getorderquery);
        preparedStatement.setInt(1, id);
        //step4) executing the query
        ResultSet resultSet = preparedStatement.executeQuery();
        //) iterate the results
        while (resultSet.next())
        {
            System.out.println("Result's for ID = "+resultSet.getInt(1)+", Name = "+resultSet.getString(2)+", DeliveryAddress = "+resultSet.getString(3));
        }
        return null;
    }

    @Override
    public String updateOrder (Order order) throws SQLException, ClassNotFoundException
    {
        PreparedStatement preparedStatement=OrderDbConnection.getDbConnection().prepareStatement(updateQurey);
        preparedStatement.setString(1,order.getOrderName());
        preparedStatement.setString(2,order.getDeliveryAddress());
        preparedStatement.setInt(3,order.getId());
        int result= preparedStatement.executeUpdate();
        if(result ==1){
            System.out.println(order.getId()+" Record updated Successfully");
            OrderDao orderDao= new OrderDaoImpl();
            orderDao.getOrder(order.getId());
        }
        return null;
    }

    @Override
    public String deleteOrder (Integer id) throws SQLException
    {
        PreparedStatement preparedStatement=OrderDbConnection.getDbConnection().prepareStatement(deletequery);
        preparedStatement.setInt(1,id);
        int result=preparedStatement.executeUpdate();
        if (result==1){
            System.out.println("ID = "+id+" Deleted Successfully");
        }
        else{
            System.out.println("Record not found");
        }
        return null;
    }
}
