package com.flipkrt.orders.service;

import com.flipkrt.orders.beans.Order;

import java.sql.SQLException;

public interface OrderService {

    String getAllOrders() throws SQLException;
    String createOrder(Order order) throws SQLException, ClassNotFoundException;
    Order getOrder(Integer id) throws SQLException, ClassNotFoundException;
    String updateOrder(Order order)throws SQLException, ClassNotFoundException;
    String deleteOrder(Integer id)throws SQLException;

}
