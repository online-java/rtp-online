package com.flipkrt.orders.dao;

import com.flipkrt.orders.beans.Order;

import java.sql.SQLException;

public interface OrderDao
{
    String getAllOrders () throws SQLException;
    String createOrder (Order order) throws ClassNotFoundException, SQLException;
    Order getOrder (Integer id) throws ClassNotFoundException, SQLException;
    String updateOrder(Order order) throws SQLException, ClassNotFoundException;
    String deleteOrder (Integer id) throws SQLException;
} 
