package com.flipkrt.orders.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class OrderDbConnection
{

    private static final String username = "root";
    private static final String password = "root";
    private static final String database = "orderdb12";
    private static final String dburl = "jdbc:mysql://localhost:3306/";
    private static Connection connection = null;

    public static Connection getDbConnection ()
    {
        try
        {
            //database ......
            //step1) registering driver
            //Class.forName("com.mysql.jdbc.Driver");
            //step2) opening the connection
            connection = DriverManager.getConnection(dburl + database, username, password);
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return connection;
    }
}
