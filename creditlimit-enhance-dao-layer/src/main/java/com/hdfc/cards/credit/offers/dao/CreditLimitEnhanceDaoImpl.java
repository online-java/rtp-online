package com.hdfc.cards.credit.offers.dao;

import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.dao.util.CreditLimitEnhanceDaoJdbcUtility;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.CleEnum;
import com.hdfc.cards.credit.offers.exception.SystemException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditLimitEnhanceDaoImpl implements CreditLimitEnhanceDaoI {
    @Override
    public ClePromocodeDaoResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {
        final Logger daoLogger = Logger.getLogger(CreditLimitEnhanceDaoImpl.class);
        //System.out.println("Entered into dao layer verifyPromocode{}");
        daoLogger.debug("Entered into DAO layer verifyPromocode{}");
        ClePromocodeDaoResponse clePromocodeDaoResponse = null;
        //call to Database and get thee ressponse

        Connection connection = CreditLimitEnhanceDaoJdbcUtility.getDbConnection();

        String query = "Select * from clepromocodeinfo where promocode=?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, promocde);

            ResultSet resultSet = preparedStatement.executeQuery();

            clePromocodeDaoResponse = null;

            /*     System.out.println("row size : "+ resultSet.getRow());
             */
            if (resultSet.next()) {
                clePromocodeDaoResponse = new ClePromocodeDaoResponse();
                clePromocodeDaoResponse.setPromocode(resultSet.getString(1));
                clePromocodeDaoResponse.setCurrentLimit(resultSet.getDouble(2));
                clePromocodeDaoResponse.setEligibleAmount(resultSet.getDouble(3));
                clePromocodeDaoResponse.setExpDate(resultSet.getString(4));

            } else {

                throw new CleDataAccessException(CleEnum.PROMOCODE_DETAILS_NOT_FOUND);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SystemException(CleEnum.INTERNAL_SERVER_ERROR);

        }
        daoLogger.debug("Exiting from DAO layer verifyPromocode{}");
        return clePromocodeDaoResponse;
    }

    public static void main(String[] args) {

/*
        CreditLimitEnhanceDaoImpl creditLimitEnhanceDao = new CreditLimitEnhanceDaoImpl();
        System.out.println(creditLimitEnhanceDao.verifyPromocode("hdfc2020"));*/

    }
}
