package com.hdfc.cards.credit.offers.dao.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CreditLimitEnhanceDaoJdbcUtility {
    private static final String username = "root";
    private static final String password = "root";
    private static final String database = "cledb2020";
    private static final String dburl = "jdbc:mysql://localhost:3306/";

    public static Connection getDbConnection() {
        Connection connection = null;
        //database ......
        //step1) registering drivaer
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //step2) opening the connection
            connection = DriverManager.getConnection(dburl + database, username, password);
        } catch (SQLException | ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return connection;
    }


}
