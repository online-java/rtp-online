package com.hdfc.cards.credit.offers.dao;

import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.dao.util.CreditLimitEnhanceDaoJdbcUtility;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CreditLimitEnhanceDaoImpl.class,CreditLimitEnhanceDaoJdbcUtility.class})
public class CreditLimitEnhanceDaoImplTest
{
    private CreditLimitEnhanceDaoImpl creditLimitEnhanceDao;
    private ResultSet resultSet;
    private PreparedStatement preparedStatement;
    private Connection connection;

    @Before
    public void setUp ()  
    {
        creditLimitEnhanceDao= new CreditLimitEnhanceDaoImpl();
        resultSet= PowerMockito.mock(ResultSet.class);
        preparedStatement=PowerMockito.mock(PreparedStatement.class);
        connection= PowerMockito.mock(Connection.class);

    }

    @Test
    public void verifyPromocode () throws CleDataAccessException, SystemException, SQLException
    {
        String promocode="hdfc2040";
        String query="Select * from clepromocodeinfo where promocode=?";
       PowerMockito.mockStatic(DriverManager.class);
       PowerMockito.when(DriverManager.getConnection(Mockito.anyString(),Mockito.anyString(),Mockito.anyString())).thenReturn(connection);
       PowerMockito.when(connection.prepareStatement(Mockito.anyString())).thenReturn(preparedStatement);
       preparedStatement.setString(1,promocode);
       PowerMockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
       PowerMockito.when(resultSet.next()).thenReturn(true);
        PowerMockito.when(resultSet.getString(1)).thenReturn("hdfc2040");
        PowerMockito.when(resultSet.getDouble(2)).thenReturn(5000d);
        PowerMockito.when(resultSet.getDouble(3)).thenReturn(1000d);
        PowerMockito.when(resultSet.getString(4)).thenReturn("date");
        ClePromocodeDaoResponse clePromocodeDaoResponse=creditLimitEnhanceDao.verifyPromocode(promocode);
        Mockito.verify(connection).prepareStatement(query);
        Mockito.verify(preparedStatement).executeQuery();
        Mockito.verify(resultSet).next();
        Mockito.verify(resultSet).getString(1);
        Mockito.verify(resultSet).getDouble(2);
        Mockito.verify(resultSet).getDouble(3);
        Mockito.verify(resultSet).getString(4);
        PowerMockito.verifyStatic();
        Double expcurrentLimit=5000d;
        Double expEligibleLimit=1000d;
        Assert.assertEquals("hdfc2040",clePromocodeDaoResponse.getPromocode());
        Assert.assertEquals(expcurrentLimit,clePromocodeDaoResponse.getCurrentLimit());
        Assert.assertEquals(expEligibleLimit,clePromocodeDaoResponse.getEligibleAmount());
        Assert.assertEquals("date",clePromocodeDaoResponse.getExpDate());
    }
   
    @After
    public void tearDown () 
    {
        creditLimitEnhanceDao= null;
        connection=null;
        preparedStatement= null;
        resultSet= null;
    }
}