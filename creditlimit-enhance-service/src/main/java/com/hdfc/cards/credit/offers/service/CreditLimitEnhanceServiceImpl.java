package com.hdfc.cards.credit.offers.service;

import com.hdfc.cards.credit.offers.dao.CreditLimitEnhanceDaoI;
import com.hdfc.cards.credit.offers.dao.CreditLimitEnhanceDaoImpl;
import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.SystemException;
import com.hdfc.cards.credit.offers.service.dto.ClePromocodeServiceResponse;
import org.apache.log4j.Logger;

public class CreditLimitEnhanceServiceImpl implements CreditLImitEnhanceServiceI {

    private CreditLimitEnhanceDaoI creditLimitEnhanceDao = null;
    final org.apache.log4j.Logger serviceLogger = Logger.getLogger(CreditLimitEnhanceDaoImpl.class);

    @Override
    public ClePromocodeServiceResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException {
        creditLimitEnhanceDao = new CreditLimitEnhanceDaoImpl();
        // PowerMockito.whenNew(CreditLimitEnhanceDaoImpl.class).withNoArguments().thenReturn(creditLimitEnhanceDaoMock);
        //System.out.println("Entered into service layer verifyPromocode{}");
        serviceLogger.debug("Entered into service layer verifyPromocode{}");
        ClePromocodeServiceResponse clePromocodeServiceResponse = null;
        // call to dao layer and get ClePromocodeDaoResponse
        ClePromocodeDaoResponse clePromocodeDaoResponse = creditLimitEnhanceDao.verifyPromocode(promocde);

        clePromocodeServiceResponse = new ClePromocodeServiceResponse();

        clePromocodeServiceResponse.setCurrentLimit(clePromocodeDaoResponse.getCurrentLimit());
        clePromocodeServiceResponse.setEligibleAmount(clePromocodeDaoResponse.getEligibleAmount());
        clePromocodeServiceResponse.setExpDate(clePromocodeDaoResponse.getExpDate());
        clePromocodeServiceResponse.setPromocode(clePromocodeDaoResponse.getPromocode());

        serviceLogger.debug("Exiting  from service layer verifyPromocode{}");
        //System.out.println("Exiting  from service layer verifyPromocode{}");

        return clePromocodeServiceResponse;
    }
}
