package com.hdfc.cards.credit.offers.service;

import com.hdfc.cards.credit.offers.dao.CreditLimitEnhanceDaoImpl;
import com.hdfc.cards.credit.offers.dao.dto.ClePromocodeDaoResponse;
import com.hdfc.cards.credit.offers.exception.CleDataAccessException;
import com.hdfc.cards.credit.offers.exception.CleEnum;
import com.hdfc.cards.credit.offers.service.dto.ClePromocodeServiceResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CreditLimitEnhanceServiceImpl.class)
public class CreditLimitEnhanceServiceImplTest {

    private CreditLImitEnhanceServiceI creditLImitEnhanceService;
    private CreditLimitEnhanceDaoImpl creditLimitEnhanceDaoMock;

    @Before
    public void setUp() throws Exception {

        creditLImitEnhanceService = new CreditLimitEnhanceServiceImpl();
        creditLimitEnhanceDaoMock = PowerMockito.mock(CreditLimitEnhanceDaoImpl.class);
    }

    @Test
    public void verifyPromocode() throws Exception {
        PowerMockito.whenNew(CreditLimitEnhanceDaoImpl.class).withNoArguments().thenReturn(creditLimitEnhanceDaoMock);
        ClePromocodeDaoResponse clePromocodeDaoResponse = new ClePromocodeDaoResponse();
        clePromocodeDaoResponse.setCurrentLimit(50900d);
        clePromocodeDaoResponse.setEligibleAmount(1000d);
        clePromocodeDaoResponse.setExpDate("");
        clePromocodeDaoResponse.setPromocode("HDFC2020");
        PowerMockito.when(creditLimitEnhanceDaoMock.verifyPromocode("HDFC2020")).thenReturn(clePromocodeDaoResponse);
        ClePromocodeServiceResponse clePromocodeServiceResponse = creditLImitEnhanceService.verifyPromocode("HDFC2020");
        Assert.assertEquals(50900d, (double) clePromocodeServiceResponse.getCurrentLimit(), 1);
        Assert.assertEquals(1000, clePromocodeServiceResponse.getEligibleAmount(), 1);
        Assert.assertEquals("", clePromocodeServiceResponse.getExpDate());
        Assert.assertEquals("HDFC2020", clePromocodeServiceResponse.getPromocode());

    }

    @Test(expected = CleDataAccessException.class)
    public void verifyPromocodeWithInvalidInput() throws Exception {
        PowerMockito.whenNew(CreditLimitEnhanceDaoImpl.class).withNoArguments().thenReturn(creditLimitEnhanceDaoMock);

        CleDataAccessException cleDataAccessException = new CleDataAccessException(CleEnum.PROMOCODE_DETAILS_NOT_FOUND);

        PowerMockito.when(creditLimitEnhanceDaoMock.verifyPromocode("HDFC2021")).thenThrow(cleDataAccessException);
        ClePromocodeServiceResponse clePromocodeServiceResponse = creditLImitEnhanceService.verifyPromocode("HDFC2021");


    }


}