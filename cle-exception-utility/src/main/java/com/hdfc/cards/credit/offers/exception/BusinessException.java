
package com.hdfc.cards.credit.offers.exception;

import javax.xml.bind.annotation.XmlRootElement;

public class BusinessException extends Exception {

    public BusinessException(){

    }
    private String errorMessage;
    private String errorCode;

    public BusinessException(CleEnum cleEnum) {
        this.errorMessage = cleEnum.getMessage();
        this.errorCode = cleEnum.getCode();

    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
