package com.hdfc.cards.credit.offers.exception;

public class CleDataAccessException extends Exception {


    private String errorMessage;
    private String errorCode;

    public CleDataAccessException(CleEnum cleEnum) {

        this.errorMessage = cleEnum.getMessage();
        this.errorCode = cleEnum.getCode();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
