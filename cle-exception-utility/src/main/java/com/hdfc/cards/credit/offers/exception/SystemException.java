package com.hdfc.cards.credit.offers.exception;

public class SystemException extends  Exception {

    private String errorMessage;
    private String errorCode;

    public SystemException(CleEnum cleEnum) {
        this.errorMessage = cleEnum.getMessage();
        this.errorCode = cleEnum.getCode();

    }

    public String getErrorMessage() {
        return errorMessage;
    }


    public String getErrorCode() {
        return errorCode;
    }

}
