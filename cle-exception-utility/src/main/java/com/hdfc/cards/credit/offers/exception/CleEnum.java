package com.hdfc.cards.credit.offers.exception;
/*

 */
public enum CleEnum {

    PROMOCODE_IS_NOT_VALID("promocode is not valid", "CLE1001"),
    PROMOCODE_DETAILS_NOT_FOUND("no data found for given promocode", "CLE1002"),
    INTERNAL_SERVER_ERROR("something went wrong", "CLE1003");

    private String message;
    private String code;

    CleEnum(String message, String code) {
        this.message = message;
        this.code = code;


    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
